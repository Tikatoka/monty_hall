def test_solution(coconut_count, num_sailors):
    for i in range(num_sailors):
        if coconut_count % num_sailors == 1:
            coconut_count = int((coconut_count - 1) * (num_sailors-1) / num_sailors)
        else:
            return False
    return coconut_count % num_sailors == 0


def get_solution(num_sailors):
    max_coconut = 1000000
    for i in range(num_sailors, max_coconut):
        if test_solution(i, num_sailors):
            return i
    return "Didn't find solution for at least {0} coconuts".format(max_coconut)


if __name__ == '__main__':
    num_sailors = 6
    result = get_solution(num_sailors)
    if isinstance(result, int):
        print(get_solution(num_sailors),'coconuts minimum')
        for i in range(num_sailors):
            portion = int((result - 1) / num_sailors)
            result -= portion
            print ('Sailor {0} hides {1} coconuts'.format(i+1, portion))
    else:
        print('no solution')