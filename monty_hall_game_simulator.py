import random
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use('TkAgg')


def one_game(num_doors, num_openDoor, autoPlay=False, switch=True):
    all_doors = [i for i in range(1, num_doors+1)]
    car_position = random.randint(1, num_doors)
    if autoPlay:
        choice = random.choice(all_doors)
    else:
        choice = int(input('Please select a number from 1 to {0}: '.format(num_doors)))

    all_doors.remove(choice)
    if choice != car_position:
        all_doors.remove(car_position)
    for i in range(num_openDoor):
        open_one_door = random.choice(all_doors)
        all_doors.remove(open_one_door)

    if not switch:
        return choice == car_position
    elif autoPlay:
        posible_choice = set(all_doors)
        posible_choice.add(car_position)
        new_choice = random.choice(list(posible_choice))
        return new_choice == car_position
    else:
        new_choice = -1
        posible_choice = set(all_doors)
        posible_choice.add(car_position)
        while new_choice not in posible_choice:
            new_choice = int(input('Choose one from the closed open as your new choice: ' + str(posible_choice)))
        return new_choice == car_position

def monte_carlo (num_doors, num_openDoor, num_games):
    switch = {
        "win_game": 0,
        "lose_game": 0
    }

    stay = {
        "win_game": 0,
        "lose_game": 0
    }

    # switch result
    for i in range(num_games):
        result_switch = one_game(num_doors, num_openDoor, autoPlay=True, switch=True)
        result_stay = one_game(num_doors, num_openDoor, autoPlay=True, switch=False)
        if result_switch:
            switch["win_game"] += 1
        else:
            switch["lose_game"] += 1

        if result_stay:
            stay["win_game"] += 1
        else:
            stay["lose_game"] += 1

    return switch["win_game"]/(switch["win_game"] + switch["lose_game"]), \
           stay["win_game"]/(stay["win_game"] + stay["lose_game"])


def statistics():
    num_doors = 10
    num_games = num_doors * 10
    switch_win_rate = []
    stay_win_rate = []
    x_range = []
    for i in range (1, num_doors-1):
        x_range.append(i)
        switch,  stay = monte_carlo(num_doors, i, num_games)
        switch_win_rate.append(switch)
        stay_win_rate.append(stay)
    plt.xlabel('number of open doors')
    plt.ylabel('Win rate')
    plt.title('{0} simulations for {1} doors'.format(num_games, num_doors))
    p1 = plt.plot(x_range, switch_win_rate, 'r', linewidth=2.0, label="Switch")
    p2 = plt.plot(x_range, stay_win_rate, 'g', linewidth=2.0, label="Stay")
    plt.legend(loc=2)
    # plt.legend()
    plt.show()


if __name__ == '__main__':

    # num_doors = 1000
    # num_open_doors = 980
    # num_games = 10000
    # switch, stay = monte_carlo(num_doors, num_open_doors, num_games)
    # print('With {0} games, switch gives a winning rate of {1} and stay is {2}'.format(num_games, switch, stay))
    statistics()